package com.example.swarnim.smartnotes;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteStatement;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class SpeechToTextActivity extends AppCompatActivity {

    Button micButton;
    EditText textFromSpeech;

    SQLiteDatabase smartNotesDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech_to_text);

        micButton = (Button) findViewById(R.id.micButton);
        textFromSpeech = (EditText) findViewById(R.id.textFromSpeech);

        smartNotesDB = this.openOrCreateDatabase("Smartnotes", MODE_PRIVATE, null);

        micButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speechToText();
            }
        });

    }

    public void speechToText(){

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Say something");
        try {
            startActivityForResult(intent, 100);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Speech Not Supported!",
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == 100){
            ArrayList<String> textString = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            textFromSpeech.setText(textString.get(0));

            try {
                smartNotesDB.execSQL("CREATE TABLE IF NOT EXISTS smartnotes (id INTEGER PRIMARY KEY, notes VARCHAR)");

                String sql = "INSERT INTO smartnotes (notes) VALUES (?)";

                SQLiteStatement statement = smartNotesDB.compileStatement(sql);

                statement.bindString(1, textString.get(0));

                statement.execute();

                Cursor c = smartNotesDB.rawQuery("SELECT DISTINCT * FROM smartnotes", null);

                int notesIndex = c.getColumnIndex("notes");
                int keyIndex = c.getColumnIndex("id");

                c.moveToFirst();

                while (c != null) {

                    Log.i("Result_string", c.getString(notesIndex));
                    Log.i("Result_id", Integer.toString(keyIndex));

                    c.moveToNext();
                }

            } catch (Exception e){
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
